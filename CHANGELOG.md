# Change Log

All noteable changes to this project will be documented in this file. This project adheres to http://semver.org/[Semantic Versioning]

## [ 0.3.0 ] – 2019-11-07

- only convert if original file is newer or if the force flag (-f) was set
- fix copy paste error in license file

## [ 0.2.1 ] – 2019-08-30

### Fixed

- Replaced hardcoded .convert.json path with configured path

## [ 0.2.0 ] – 2019-08-30

- Read configuration from .convert.json
- Fix documentation
- Add usage information / help

### Fixed

- Fix image orientation vs. geometry (max dimensions width / height were swapped before, if the image had EXIF orientation)

## [ 0.1.0 ] – 2019-08-27

Initial version
