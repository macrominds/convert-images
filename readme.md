# convert-images

Converts large images to configurable dimensions, reduces file size as much as possible while keeping the images as crisp as possible.

Creates jpg files as well as webp.

This bash script is the result of years of experiments and research.

The script grew over time and since the process has involved a lot of trial and error, it lacks explanations for why certain flags have been set or certain approaches have been taken.

## Usage

Download [convert-images.sh](./src/convert-images.sh) and make it executable `chmod u+x convert-images.sh`. 
Add it to your `$PATH` or save it in one of your `$PATH` folders so that you can easily execute it.

Prepare a `.convert.json` file that controls how the images will be converted and where they will be stored. See [.convert.json](.convert.json) as an example.

```json
[
	{
		"target": "images/numeric-fit-width",
		"geometry": 1632,
		"sharpness": 100,
		"quality": 45
	},
	…
	{
		"target": "images/widthxheight-fit-box",
		"geometry": "800x600",
		"sharpness": 100,
		"quality": 45
	},
	…
]
```

Converted images are stored in the `target` folder. `target` can be defined relative to the current working directory.

`geometry` is supposed to match one of the following patterns: 

* `width`, i.e. `1632`, `"1632"` or `"1632x"` for 1632px max width
* `widthxheight`, i.e. `"1920x1920"` to fit a box of 1920×1920 px
* `xheight`, i.e. `"x1920"` to for 1920px max height

`sharpness` defines the sharpness of the resulting image in percent from 0–100. The higher the number, the sharper the image.

`quality` defines the tradeoff between file size and jpg artefacts in percent from 0–100. The higher the number, the larger the file, the better the visual quality.

The generated `.webp` images ignore the quality setting and are usually remarkably smaller than the optimized jpg.

Run the script as follows:

```bash
convert-images.sh path/to/images/folder/*.jpg
```

By default, a `.config.json` file is expected in the current working directory. However, you can also specify the path to your `.config.json`:

```bash
convert-images.sh -c my.config.json img/*.jpg
```

To force the conversion even if the source file is older than the respective converted files, run

```bash
convert-images.sh -f -c my.config.json img/*.jpg
```

## TODO

- add a system requirements check and instructions (imagemagick, webp, jq)
- improve handling of temporary files
- fix color profile extraction and re-insertion

## Author

Thomas Praxl <kontakt@macrominds.de>
