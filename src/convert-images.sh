#!/usr/bin/env bash
############################################################################
#
# Version 0.3.0
#
# Usage: convert-images [option]... [file]...
#
# Scale images while keeping them as crisp as possible and optimize size.
# Creates jpg and webp.
# Is controlled by a .convert.json file whose path can be specified with
# the -c option.
#
# Requires imagemagick, webp and jq
#
# Options:
#  -h               ... help message
#  -c               ... config file path
#  -f               ... force conversion even if the original has not changed
#
#############################################################################



# exit when a command fails
set -o errexit
# return the exit status of the last command that threw a non-zero exit code
set -o pipefail
# exit when script tries to use undeclared variables
set -o nounset

main() {
  #defaults
  local convert_json_path='.convert.json'
  local force_conversion=0
  #getopts initialization for usage in function
  local OPTIND opt


  while getopts ":hc:f" opt; do
    case "${opt}" in
      h)
        print_usage
        exit
        ;;
      c)
        convert_json_path="${OPTARG}"
        ;;
      f)
        force_conversion=1
        ;;
      \?)
        print_error "Invalid option: ${OPTARG}" >&2
        print_usage
        exit 1
        ;;
    esac
  done

  shift "$((OPTIND-1))"

  if [ ! -f "${convert_json_path}" ]; then
    print_error "The configuration file $(realpath "${convert_json_path}") could not be found."
    print_usage
    exit 1
  fi

  if [ ${#} -lt 1 ] ; then
    print_error "You need to specify one or more files."
    print_usage
    exit 1
  fi

  for FILE in "$@"
  do
    COUNT=$(jq -r 'length' "${convert_json_path}")
    for ((i=0; i<$COUNT; i++))
    do
      declare -A SETTINGS="($(jq --arg i $i -r '.[$i|tonumber] | "[geometry]=\"" + (.geometry|tostring) + "\" [sharpness]=\"" + (.sharpness|tostring) + "\" [quality]=\"" + (.quality|tostring) + "\" [target]=\"" + (.target|tostring) + "\""' "${convert_json_path}"))"
      GEOMETRY="${SETTINGS[geometry]}"
      SHARPNESS="${SETTINGS[sharpness]}"
      QUALITY="${SETTINGS[quality]}"
      TARGET="${SETTINGS[target]}"
      convert_file "${FILE}" $GEOMETRY $SHARPNESS $QUALITY $TARGET $force_conversion
    done
  done
  exit
}

print_usage() {
  echo "
$(cat "$0" | gawk '/^####/,/^$/')
"
}

print_hint()
{
  echo -e "\033[93m${1}\033[0m"
}

print_error()
{
  (>&2 echo -e "\033[91m${1}\033[0m")
}

function convert_file() {
  { #try
    FULL_FILE_NAME="${1}"
    GEOMETRY="${2}"
    SHARPNESS="${3}"
    QUALITY="${4}"
    FILE_PATH="${5}"
    FORCE_CONVERSION=${6}

    FILE_NAME="${FULL_FILE_NAME##*/}"

    ICC_NAME="${FILE_PATH}/IMsource.icc"

    # RGB OR sRGB (sRGB produces deeper and darker colors as it seems)
    WORKING_COLORSPACE=RGB

    [[ -d $FILE_PATH ]] || mkdir -p $FILE_PATH
    
    local uncompressed_jpg_target="${FILE_PATH}/${FILE_NAME}.uncompressed"
    local compressed_jpg_target="${FILE_PATH}/${FILE_NAME}"
    local webp_target="${FILE_PATH}/${FILE_NAME%.*}.webp"
    local jpg_conversion_required=0
    if [[ $FORCE_CONVERSION = 1 ]] || [[ ! -f "${compressed_jpg_target}" ]] || [[ "${FULL_FILE_NAME}" -nt "${compressed_jpg_target}" ]]; then
      jpg_conversion_required=1
    fi

    local webp_conversion_required=0
    if [[ $FORCE_CONVERSION = 1 ]] || [[ ! -f "${webp_target}" ]] || [[ "${FULL_FILE_NAME}" -nt "${webp_target}" ]]; then
      webp_conversion_required=1
    fi

    # TODO Correctly extract color profile
    #
    # See http://www.imagemagick.org/Usage/formats/#profiles
    # identify -verbose ${FULL_FILE_NAME} | grep 'Profile-.*bytes'
    # convert convert -define jpeg:size=64x64  image.jpg  iptc:"${FILE_PATH}/${ICC_NAME}"

    if [ $jpg_conversion_required != 0 ] || [ $webp_conversion_required != 0 ]; then
      # Lossless scale
      convert \( \
            "${FULL_FILE_NAME}" \
            -type TrueColor \
            -depth 16 \
            -set colorspace sRGB \
            -colorspace ${WORKING_COLORSPACE} \
            -define filter:c=0.1601886205085204 \
            -filter Cubic \
            -auto-orient \
            -distort Resize "$GEOMETRY" \
          \) \
          \( \
            -clone 0 \
            -gamma 3 \
            -define convolve:scale=${SHARPNESS}%,100 \
            -morphology Convolve DoG:3,0,0.4981063336734057 \
            -gamma 0.3333333333333333333 \
          \) \
          \( \
            -clone 0 \
            -define convolve:scale=${SHARPNESS}%,100 \
            -morphology Convolve DoG:3,0,0.4806768770037563 \
          \) \
          -delete 0 \
          \( \
            -clone 1 \
            -colorspace gray \
            -auto-level \
          \) \
          -compose over \
          -composite \
          -set colorspace ${WORKING_COLORSPACE} \
          -colorspace sRGB \
          -quality 100% \
          -sampling-factor 4:4:4 \
          -define png:preserve-iCCP \
          -auto-orient \
          -compress Lossless "${uncompressed_jpg_target}"
      # Compress jpg
      convert -interlace None \
        -quality ${QUALITY}% \
        "${uncompressed_jpg_target}" \
        "${compressed_jpg_target}"

      # Create webp
      cwebp "${uncompressed_jpg_target}" -progress -o "${webp_target}"

      # Cleanup
      rm "${uncompressed_jpg_target}"
    fi

  } || { #catch
    echo "An error occured during conversion to ${FILE_PATH}/${FILE_NAME}. This might not be fatal."
  }
}

main "$@"